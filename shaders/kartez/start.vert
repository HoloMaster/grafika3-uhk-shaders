#version 150
in vec2 inPosition; // vstup do vertex shaderu z aplikace

uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightViewProjection;
uniform vec3 lightFromCameraLight;

uniform float time;
uniform float type; // type == 1 -> elipsoid; type == 0 -> stěna

out vec3 normal;
out vec3 light;
out vec3 viewDirection;
out vec4 depthTextureCoord;
out vec2 texCoord;
out vec3 vertColor;
out vec3 lightDirection;
out float intensity;
out vec3 spotDirection;
out vec3 lightFromLight;

const float PI = 3.1415;


vec3 getSun(vec2 vec){
    float az = vec.x * 3.14;
    float ze = vec.y * 3.14 / 2;
    float r = .3;

    float x = r * cos(az) * cos(ze);
    float y = r * sin(az) * cos(ze);
    float z = r * sin(ze);

    return vec3(x,y,z);
}
// ohnutí gridu pomocí funkčního předpisu
float getZ(vec2 vec) {
    return sin(time + vec.y * PI * 2);
}

// ohnutí gridu do podoby elipsoidu
vec3 getSphere(vec2 vec) {
    float az = vec.x * PI;
    float ze = vec.y * PI / 2; // souřadnice tady máme od -1 do 1 a chceme od -PI/2 do PI/2
    float r = 1;

    float x = r * cos(az) * cos(ze);
    float y = 2 * r * sin(az) * cos(ze);
    float z = 0.5 * r * sin(ze);
    return vec3(x, y, z);
}

// výpočet normál pomocí parciálních derivací
vec3 getSphereNormal2(vec2 xy) {
    float az = xy.x * PI;
    float ze = xy.y * PI/2;

    vec3 dx = vec3(-sin(az)*cos(ze)*PI, cos(az)*cos(ze) * PI, 0);
    vec3 dy = vec3(cos(az)*-sin(ze)*PI/2, sin(az)*-sin(ze) * PI/2, cos(ze)*PI/2);
    return cross(dx, dy);
}

// výpočet normál pomocí diferencí
vec3 getSphereNormal(vec2 vec) {
    vec3 u = getSphere(vec + vec2(0.001, 0)) - getSphere(vec - vec2(0.001, 0));
    vec3 v = getSphere(vec + vec2(0, 0.001)) - getSphere(vec - vec2(0, 0.001));
    return cross(u, v);
}

vec3 getPlane(vec2 vec) {
    return vec3(vec.x * 2.5, vec.y * 2.5,(sin(time + vec.x * 3.14)/2) * cos(time + vec.y * 3.14)/2-2);
}

vec3 getPlaneNormal(vec2 vec) {
    vec3 u = getPlane(vec + vec2(0.001, 0)) - getPlane(vec - vec2(0.001, 0));
    vec3 v = getPlane(vec + vec2(0, 0.001)) - getPlane(vec - vec2(0, 0.001));
    return cross(u, v);
}

void main() {
    // grid máme od 0 do 1 a chceme od -1 do 1 (funkce pro ohyb gridu s tím počítají)
    vec2 position = inPosition * 2 - 1;
    //vec4 pos4 = vec4(pos, getZ(pos), 1.0);
    vec4 pos4;
    if (type == 1) {
        pos4 = vec4(getSphere(position), 1.0);
        normal = mat3(view) * getSphereNormal(position);
        // normal = pos4.xyz; // speciální případ pro kouli umístěnou v počátku
    } else if (type == 0) {
        pos4 = vec4(getPlane(position), 1.0);
        normal = mat3(view) * getPlaneNormal(position);
    } else if( type == 2){
        pos4 = vec4(getSun(position), 1.0);
        pos4.x =pos4.x + lightFromCameraLight.x;
        pos4.y =pos4.y + lightFromCameraLight.y;
        pos4.z =pos4.z + lightFromCameraLight.z;
    }

    gl_Position = projection * view * pos4;

    vec3 lightPos = -lightFromCameraLight;
    light = lightPos - (view * pos4).xyz;

    lightFromLight = lightPos - (view * pos4).xyz;

    viewDirection = - (view * pos4).xyz;

    spotDirection = -(lightViewProjection * pos4).xyz;
    //lightDirection = normalize(light-viewDirection.xyz);

    intensity = dot(normalize(light), normalize(normal));

    texCoord = inPosition;

    vertColor = pos4.xyz;

    // z pozice světla
    depthTextureCoord = lightViewProjection * pos4;
    depthTextureCoord.xyz = depthTextureCoord.xyz / depthTextureCoord.w;
    depthTextureCoord.xyz = (depthTextureCoord.xyz + 1) / 2; // obrazovka má rozsahy <-1;1>
} 
