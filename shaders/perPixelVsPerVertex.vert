#version 150
in vec2 inPosition; // vstup do vertex shaderu z aplikace

uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightViewProjection;
uniform vec3 lightFromCameraLight;

uniform float time;
uniform int type; // type == 1 -> elipsoid; type == 0 -> stěna

out vec3 normal;
out vec3 light;
out float intensity;


const float PI = 3.1415;




// ohnutí gridu do podoby elipsoidu
vec3 getSphere(vec2 vec) {
    float az = vec.x * PI;
    float ze = vec.y * PI / 2; // souřadnice tady máme od -1 do 1 a chceme od -PI/2 do PI/2
    float r = 1;

    float x = r * cos(az) * cos(ze);
    float y = 2 * r * sin(az) * cos(ze);
    float z = 0.5 * r * sin(ze);
    return vec3(x, y, z);
}

// výpočet normál pomocí diferencí
vec3 getSphereNormal(vec2 vec) {
    vec3 u = getSphere(vec + vec2(0.001, 0)) - getSphere(vec - vec2(0.001, 0));
    vec3 v = getSphere(vec + vec2(0, 0.001)) - getSphere(vec - vec2(0, 0.001));
    return cross(u, v);
}

void main() {
    // grid máme od 0 do 1 a chceme od -1 do 1 (funkce pro ohyb gridu s tím počítají)
    vec2 position = inPosition * 2 - 1;
    //vec4 pos4 = vec4(pos, getZ(pos), 1.0);
    vec4 pos4;
    if (type == 5) {
        pos4 = vec4(getSphere(position), 1.0);
        normal = mat3(view) * getSphereNormal(position);
        // normal = pos4.xyz; // speciální případ pro kouli umístěnou v počátku
    } else if (type == 6) {
        pos4 = vec4(getSphere(position), 1.0);
        pos4.x = pos4.x +2;
        normal = mat3(view) * getSphereNormal(position);
    }

    gl_Position = projection * view * pos4;

    vec3 lightPos = lightFromCameraLight;
    light = lightPos - (view * pos4).xyz;



    intensity = dot(normalize(light), normalize(normal));


} 
