#version 150
in vec2 inPosition; // input from the vertex buffer

uniform mat4 view;
uniform mat4 projection;

uniform float time;
uniform float type; // type == 1 -> elipsoid; type == 0 -> stěna
uniform int changeOfShapeLight;

const float PI = 3.1415;

// ohnutí gridu do podoby elipsoidu
vec3 getSpherical(vec2 vec){
    float az = vec.x * PI;
    float ze = vec.y * PI/2;
    float r = sin(time);

    float x = r * cos(az) * cos(ze);
    float y = r * sin(az) * cos(ze);
    float z = r * sin(ze);

    return vec3(x, y, z);
}

vec3 getSpherical2(vec2 vec){
    float az = vec.x * PI;
    float ze = vec.y * PI/2;
    float r = sin(time + ze - az) * cos(time + ze - az);

    float x = r * cos(az) * cos(ze);
    float y = r * sin(az) * cos(ze);
    float z = r * sin(ze);

    return vec3(x,y,z);
}

vec3 getPlane(vec2 vec) {
    return vec3(vec * 2.5, -2);
}

void main() {
	vec2 position = inPosition * 2 - 1; // grid máme od 0 do 1 a chceme od -1 do 1 (funkce pro ohyb gridu s tím počítají)
	vec4 pos4;
	if (type == 1.0) {
        if(changeOfShapeLight == 0){
            pos4 = vec4(getSpherical(position), 1.0);
        }else{
            pos4 = vec4(getSpherical2(position), 1.0);
        }

	} else {
	    pos4 = vec4(getPlane(position), 1.0);
	}

	gl_Position = projection * view * pos4;
} 
