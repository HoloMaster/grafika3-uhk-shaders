#version 150
in vec2 inPosition; // vstup do vertex shaderu z aplikace

uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightViewProjection;
uniform vec3 lightFromCameraLight;
uniform int changeOfShape;

uniform float time;
uniform float type; // type == 1 -> elipsoid; type == 0 -> stěna

out vec3 normal;
out vec3 light;
out vec3 viewDirection;
out vec4 depthTextureCoord;
out vec2 texCoord;
out vec3 vertColor;
out vec3 lightDirection;
out float intensity;
out vec3 spotDirection;
out vec3 lightFromLight;

const float PI = 3.1415;


vec3 getSun(vec2 vec){
    float az = vec.x * 3.14;
    float ze = vec.y * 3.14 / 2;
    float r = .3;

    float x = r * cos(az) * cos(ze);
    float y = r * sin(az) * cos(ze);
    float z = r * sin(ze);

    return vec3(x,y,z);
}
// ohnutí gridu pomocí funkčního předpisu
float getZ(vec2 vec) {
    return sin(time + vec.y * PI * 2);
}

vec3 getSpherical(vec2 vec){
    float az = vec.x * PI;
    float ze = vec.y * PI/2;
    float r = sin(time);

    float x = r * cos(az) * cos(ze);
    float y = r * sin(az) * cos(ze);
    float z = r * sin(ze);

    return vec3(x, y, z);
}

vec3 getSphericalNormal(vec2 vec){
    vec3 u = getSpherical(vec + vec2(0.001, 0))- getSpherical(vec - vec2(0.001, 0));

    vec3 v = getSpherical(vec + vec2(0, 0.001))- getSpherical(vec - vec2(0, 0.001));

    return cross(u,v);
}

vec3 getSpherical2(vec2 vec){
    float az = vec.x * PI;
    float ze = vec.y * PI/2;
    float r = sin(time + ze - az) * cos(time + ze - az);

    float x = r * cos(az) * cos(ze);
    float y = r * sin(az) * cos(ze);
    float z = r * sin(ze);

    return vec3(x,y,z);

}

vec3 getSphericalNormal2(vec2 vec){
    vec3 u = getSpherical2(vec + vec2(0.001, 0))- getSpherical2(vec - vec2(0.001, 0));

    vec3 v = getSpherical2(vec + vec2(0, 0.001))- getSpherical2(vec - vec2(0, 0.001));

    return cross(u,v);
}

vec3 getPlane(vec2 vec) {
    return vec3(vec * 2.5, -2);
}

vec3 getPlaneNormal(vec2 vec) {
    vec3 u = getPlane(vec + vec2(0.001, 0)) - getPlane(vec - vec2(0.001, 0));
    vec3 v = getPlane(vec + vec2(0, 0.001)) - getPlane(vec - vec2(0, 0.001));
    return cross(u, v);
}

void main() {
    // grid máme od 0 do 1 a chceme od -1 do 1 (funkce pro ohyb gridu s tím počítají)
    vec2 position = inPosition * 2 - 1;
    //vec4 pos4 = vec4(pos, getZ(pos), 1.0);
    vec4 pos4;
    if (type == 1) {
        if(changeOfShape == 0){
            pos4 = vec4(getSpherical(position), 1.0);
            normal = mat3(view) * getSphericalNormal(position);
        }else{
            pos4 = vec4(getSpherical2(position), 1.0);
            normal = mat3(view) * getSphericalNormal2(position);
        }

        // normal = pos4.xyz; // speciální případ pro kouli umístěnou v počátku
    } else if (type == 0) {
        pos4 = vec4(getPlane(position), 1.0);
        normal = mat3(view) * getPlaneNormal(position);
    } else if( type == 2){
        pos4 = vec4(getSun(position), 1.0);
        pos4.x =pos4.x + lightFromCameraLight.x;
        pos4.y =pos4.y + lightFromCameraLight.y;
        pos4.z =pos4.z + lightFromCameraLight.z;
    }

    gl_Position = projection * view * pos4;

    vec3 lightPos = -lightFromCameraLight;
    light = lightPos - (view * pos4).xyz;

    lightFromLight = lightPos - (view * pos4).xyz;

    viewDirection = - (view * pos4).xyz;

    spotDirection = -(lightViewProjection * pos4).xyz;
    //lightDirection = normalize(light-viewDirection.xyz);

    intensity = dot(normalize(light), normalize(normal));

    texCoord = inPosition;

    vertColor = pos4.xyz;

    texCoord = inPosition;

    // z pozice světla
    depthTextureCoord = lightViewProjection * pos4;
    depthTextureCoord.xyz = depthTextureCoord.xyz / depthTextureCoord.w;
    depthTextureCoord.xyz = (depthTextureCoord.xyz + 1) / 2; // obrazovka má rozsahy <-1;1>
} 
