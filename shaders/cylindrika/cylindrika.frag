#version 150

in vec3 normal;
in vec3 light;
in vec3 viewDirection;
in vec4 depthTextureCoord;
in vec2 texCoord;
in vec3 vertColor;
in float intensity;
in vec3 spotDirection;
in vec3 lightFromLight;

uniform sampler2D depthTexture;
uniform sampler2D textureMosaic;
uniform sampler2D textureSun;
uniform sampler2D textureGrass;
uniform float type;
uniform int mouseWheel;
uniform int reflektor;

out vec4 outColor; // (vždy jediný) výstup z fragment shaderu



void main() {
    float finalIntensity;
    float spotEffectCone = 0.9;
    float constantAttenuation=1.0;
    float linearAttenuation=0.25;
    float quadraticAttenuation=0.10;

    vec4 ambient = vec4(vec3(0.3), 1);

    float NdotL = max(0, dot(normalize(normal), normalize(light)));
    vec4 diffuse = vec4(NdotL * vec3(0.6), 1);

    vec3 halfVector = normalize(normalize(light) + normalize(viewDirection));
    float NdotH = dot(normalize(normal), halfVector);
    vec4 specular = vec4(pow(NdotH, 16) * vec3(0.8), 1);

    vec4 finalColor = ambient + diffuse + specular;
    vec4 textureColor = texture(textureMosaic, texCoord);
    if(type == 2){
        textureColor = texture(textureSun, texCoord);
    }else if(type == 0){
        textureColor = texture(textureGrass, texCoord);
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    float spotEffect = max(dot(normalize(spotDirection),normalize(-lightFromLight)),0);


    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    // "z" hodnota z textury
    // nutná dehomogenizace
    // R, G i B složky jsou stejné, protože gl_FragCoord.zzz
    float zL = texture(depthTexture, depthTextureCoord.xy).r;

    float zA = depthTextureCoord.z;

    // 0.01 - bias na ostranění tzv. akné
    // lze vyzkoušet různé hodnoty 0.01, 0.001, 0.0001, 0.00001
    bool shadow = zL < zA - 0.001;
    if (mouseWheel == 0){
        outColor = vec4(normalize(normal), 1.0);
    } else if (mouseWheel == 1){
        outColor = vec4(vertColor, 1.0);
    } else if (mouseWheel == 2){
        outColor = vec4(0, 0.49, 1.0, 1.0);
    } else if (mouseWheel == 3){
        outColor = texture(textureMosaic, texCoord);
    } else if (mouseWheel == 4){
        outColor = texture(depthTexture, depthTextureCoord.xy);
    } else if (mouseWheel == 5){
        outColor = vec4((texCoord.xy),0, 1.0);
    }else if(mouseWheel == 6){

        finalIntensity = intensity;
        vec4 color;
        if (finalIntensity>0.95)
        color=vec4(1.0, 0.5, 0.5, 1.0);
        else if (finalIntensity>0.8)
        color=vec4(0.6, 0.3, 0.3, 1.0);
        else if (finalIntensity>0.5)
        color=vec4(0.0, 0.0, 0.3, 1.0);
        else if (finalIntensity>0.25)
        color=vec4(0.4, 0.2, 0.2, 1.0);
        else
        color=vec4(0.2, 0.1, 0.1, 1.0);
        outColor = vec4(color);

    }else if(mouseWheel == 7){

        finalIntensity = dot(normalize(light), normalize(normal));

        vec4 color;
        if (finalIntensity>0.95)
        color=vec4(1.0, 0.5, 0.5, 1.0);
        else if (finalIntensity>0.8)
        color=vec4(0.6, 0.3, 0.3, 1.0);
        else if (finalIntensity>0.5)
        color=vec4(0.0, 0.0, 0.3, 1.0);
        else if (finalIntensity>0.25)
        color=vec4(0.4, 0.2, 0.2, 1.0);
        else if (finalIntensity>0)
        color=vec4(0.4, 0.2, 0.2, 1.0);
        else
        color=vec4(0.2, 0.1, 0.1, 1.0);

        outColor = vec4(color);

    }
    else if(mouseWheel == 8) {

        if (shadow) {
            outColor = ambient * textureColor;
        } else {
            if(reflektor == 1){
                if(spotEffect>spotEffectCone){
                    float blend = clamp((spotEffect-spotEffectCone)/(1-spotEffectCone),0.0,1.0);
                    outColor = mix(ambient*textureColor,finalColor*textureColor,blend);

                }else{
                    float distance = length(spotDirection);
                    float att = constantAttenuation / ((1+linearAttenuation*distance)*(1+quadraticAttenuation*distance*distance));
                    outColor = (ambient + att*(diffuse + specular)) * textureColor;
                }

            }else{
                outColor = ambient * textureColor;
            }

        }
    }
} 
