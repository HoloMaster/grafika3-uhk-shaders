#version 150
in vec2 inPosition; // input from the vertex buffer

uniform mat4 view;
uniform mat4 projection;

uniform float time;
uniform float type; // type == 1 -> elipsoid; type == 0 -> stěna
uniform int changeOfShapeLight;

const float PI = 3.1415;

// ohnutí gridu do podoby elipsoidu
vec3 getCylindric(vec2 vec){
    float t = vec.x * (-3.14);
    float s = vec.y * (3.14);

    float x = sin(t)/2 * cos(s*2);
    float y = cos(s)/2 * sin (2*t);
    float z = t;

    return vec3(x, y, z);
}

vec3 getCylindric2(vec2 vec){
    float t = vec.x * (-3.14)/3;
    float s = vec.y * (3.14)/3;

    float r =cos(tan(t)+ tan(s) +time );


    float x =r *( sin(t)/2 * cos(s*2)) ;
    float y =r * (cos(s)/2 * sin (2*t));
    float z = t;

    return vec3(x,y,z);
}
vec3 getPlane(vec2 vec) {
    return vec3(vec * 2.5, -2);
}

void main() {
	vec2 position = inPosition * 2 - 1; // grid máme od 0 do 1 a chceme od -1 do 1 (funkce pro ohyb gridu s tím počítají)
	vec4 pos4;
	if (type == 1.0) {
        if(changeOfShapeLight == 0){
            pos4 = vec4(getCylindric(position), 1.0);
        }else{
            pos4 = vec4(getCylindric2(position), 1.0);
        }

	} else {
	    pos4 = vec4(getPlane(position), 1.0);
	}

	gl_Position = projection * view * pos4;
} 
