#version 150

in vec3 normal;
in vec3 light;
in float intensity;


uniform int type;

out vec4 outColor;// (vždy jediný) výstup z fragment shaderu

void main() {
    float finalIntensity;
     if(type == 5){

            finalIntensity = intensity;
            vec4 color;
            if (finalIntensity>0.95)
            color=vec4(1.0, 0.5, 0.5, 1.0);
            else if (finalIntensity>0.8)
            color=vec4(0.6, 0.3, 0.3, 1.0);
            else if (finalIntensity>0.5)
            color=vec4(0.0, 0.0, 0.3, 1.0);
            else if (finalIntensity>0.25)
            color=vec4(0.4, 0.2, 0.2, 1.0);
            else
            color=vec4(0.2, 0.1, 0.1, 1.0);
            outColor = vec4(color);

    }else if(type  == 6){

        finalIntensity = dot(normalize(light), normalize(normal));

        vec4 color;
        if (finalIntensity>0.95)
        color=vec4(1.0, 0.5, 0.5, 1.0);
        else if (finalIntensity>0.8)
        color=vec4(0.6, 0.3, 0.3, 1.0);
        else if (finalIntensity>0.5)
        color=vec4(0.0, 0.0, 0.3, 1.0);
        else if (finalIntensity>0.25)
        color=vec4(0.4, 0.2, 0.2, 1.0);
        else if (finalIntensity>0)
        color=vec4(0.4, 0.2, 0.2, 1.0);
        else
        color=vec4(0.2, 0.1, 0.1, 1.0);

        outColor = vec4(color);

    }

} 
