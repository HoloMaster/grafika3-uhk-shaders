package main;

import lwjglutils.*;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.*;
import transforms.*;

import java.io.IOException;
import java.nio.DoubleBuffer;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;

/**
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2019-09-02
 */
public class Renderer extends AbstractRenderer {

    private int shaderProgramViewer, shaderProgramLight;
    private OGLBuffers buffers;
    private int locView, locProjection, locTime, locType, locLightVP,locReflektor;
    private int locViewLight, locProjectionLight, locTimeLight, locTypeLight;

    private Camera camera, cameraLight;
    private Mat4PerspRH projection;
    private float time;

    private OGLRenderTarget renderTarget;
    private OGLTexture2D.Viewer viewer;
    private OGLTexture2D textureMosaic, textureSun;
    private int locLightFromCamera;
    private int locLightFromCameraLight;
    private int reflektor;
    private String text;

    private boolean OrthoOrProjection;
    private Mat4OrthoRH ortho;
    private Mat4 hybridMatForView;
    private boolean isMousePressed, isMousePressed2, animation = true;
    private double mx, my;
    private int mouseWheel = 8;
    private int mouvingSunCounter;
    private float movingSun = 0.01f;
    private int changeOfShape = 0;
    private int locChangeOfShape;
    private int locChangeOfShapeLight;
    private int locmouseWheel;
    private OGLBuffers bufferForPPVsPV;
    private OGLTextRenderer textRenderer;
    private boolean viewerNoFill = true;
    private String popisBarvy, popisShaderu;
    private OGLTexture2D textureGrass;
    private int polygonMode;

    @Override
    public void init() {
        glClearColor(0.1f, 0.1f, 0.1f, 1);
        glEnable(GL_DEPTH_TEST);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        shaderProgramViewer = ShaderUtils.loadProgram("/kartez/start");
        shaderProgramLight = ShaderUtils.loadProgram("/kartez/light");
        popisShaderu = "kartezske";
        calculateShaderLocs(shaderProgramViewer, shaderProgramLight);


        buffers = GridFactory.generateGridStrip(100, 100);
        bufferForPPVsPV = GridFactory.generateGridStrip(10, 10);
        textRenderer = new OGLTextRenderer(width, height);
        renderTarget = new OGLRenderTarget(1024, 1024);
        viewer = new OGLTexture2D.Viewer();


        text = new String(": [LMB] camera, WSAD");
        popisBarvy = "Stiny + reflektor";
        textRenderer.clear();
        textRenderer.addStr2D(3, 20, text);
        textRenderer.addStr2D(width - 90, height - 3, " (c) PGRF UHK");


        try {
            textureMosaic = new OGLTexture2D("./textures/paper.jpg");
            textureSun = new OGLTexture2D("./textures/sun.jpg");
            textureGrass = new OGLTexture2D("./textures/grass.jpg");

        } catch (IOException e) {
            e.printStackTrace();
        }

        cameraLight = new Camera()
                .withPosition(new Vec3D(0, 0, 8))
                .withAzimuth(1.49923f * Math.PI)
                .withZenith(-0.48701 * Math.PI);

        camera = new Camera()
                .withPosition(new Vec3D(4, 4, 4))
                .withAzimuth(5 / 4f * Math.PI)
                .withZenith(-1 / 5f * Math.PI)
                .withFirstPerson(false)
                .withRadius(6);

        projection = new Mat4PerspRH(
                Math.PI / 3,
                Window.HEIGHT / (float) Window.WIDTH,
                1,
                20
        );
        hybridMatForView = projection;
    }

    @Override
    public void display() {
        if(animation){
        time += 0.1;}
        if(polygonMode == 0){
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        else if(polygonMode == 1){

            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }
        else if(polygonMode == 2){

            glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
        }
        glUniform1i(locReflektor, 1);
        if (cameraLight.getPosition().getX() < 3 && cameraLight.getPosition().getX() > -3) {
            Vec3D temp = cameraLight.getPosition();
            cameraLight = cameraLight.withPosition(new Vec3D(temp.getX() + (movingSun), temp.getY(), temp.getZ()));
            mouvingSunCounter++;
        }
        if (mouvingSunCounter == 300) {
            movingSun = movingSun * -1;
            mouvingSunCounter = -300;
        }


        // System.out.println(cameraLight.toString());

        glUniform3f(locLightFromCameraLight, (float) camera.getPosition().getX(), (float) camera.getPosition().getY(), (float) camera.getPosition().getZ());

        renderFromLight();
        renderFromViewer();

        viewer.view(renderTarget.getColorTexture(), -1, 0, 0.5);
        viewer.view(renderTarget.getDepthTexture(), -1, -0.5, 0.5);

    }

    private void renderFromLight() {
        glUseProgram(shaderProgramLight);
        renderTarget.bind();

        glClearColor(0, 0.5f, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUniformMatrix4fv(locViewLight, false, cameraLight.getViewMatrix().floatArray());
        glUniformMatrix4fv(locProjectionLight, false, hybridMatForView.floatArray());
        glUniform1f(locTimeLight, time);
        glUniform1i(locChangeOfShapeLight, changeOfShape);
        // renderuj stěnu
        glUniform1f(locTypeLight, 0);
        buffers.draw(GL_TRIANGLE_STRIP, shaderProgramLight);

        // renderuj elipsoid
        glUniform1f(locTypeLight, 1);
        buffers.draw(GL_TRIANGLE_STRIP, shaderProgramLight);
    }

    private void renderFromViewer() {
        glUseProgram(shaderProgramViewer);

        // nutno opravit viewport, protože render target si nastavuje vlastní
        glViewport(0, 0, width, height);

        // výchozí framebuffer - render do obrazovky
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        glClearColor(0.5f, 0, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUniformMatrix4fv(locView, false, camera.getViewMatrix().floatArray());
        glUniformMatrix4fv(locProjection, false, hybridMatForView.floatArray());
        glUniformMatrix4fv(locLightVP, false, cameraLight.getViewMatrix().mul(hybridMatForView).floatArray());
        renderTarget.getDepthTexture().bind(shaderProgramViewer, "depthTexture", 1);
        textureMosaic.bind(shaderProgramViewer, "textureMosaic", 0);
        textureSun.bind(shaderProgramViewer, "textureSun",2);
        textureGrass.bind(shaderProgramViewer, "textureGrass",3);

        glUniform1f(locTime, time);
        glUniform1i(locChangeOfShape, changeOfShape);
        glUniform1i(locmouseWheel, mouseWheel);
        // renderuj stěnu
        glUniform1f(locType, 0);
        buffers.draw(GL_TRIANGLE_STRIP, shaderProgramViewer);

        // renderuj elipsoid
        glUniform1f(locType, 1);
        buffers.draw(GL_TRIANGLE_STRIP, shaderProgramViewer);

        glUniform1f(locType, 2);
        buffers.draw(GL_TRIANGLE_STRIP, shaderProgramViewer);

        glUniform1i(locType, 5);
        bufferForPPVsPV.draw(GL_TRIANGLE_STRIP, shaderProgramViewer);
        glUniform1i(locType, 6);
        bufferForPPVsPV.draw(GL_TRIANGLE_STRIP, shaderProgramViewer);

        text = new String( "[LMB] camera, WSAD");
        textRenderer.clear();
        textRenderer.addStr2D(3, 20, text);
        textRenderer.addStr2D(3,35, "[E] zmena objektu");
        textRenderer.addStr2D(3,50, "[F1-4] zmena shaderu/ teles");
        textRenderer.addStr2D(3,65, "[F,G,H] zmena formy fill/lines/point");
        textRenderer.addStr2D(3,80, "[Kolecko mysi] zmena barvy" + "[ "+popisBarvy+" ]");
        textRenderer.addStr2D(3,95, "[C] animace" + "[ "+animation+" ]");
        textRenderer.addStr2D(3,110, "[F,G,H] FILL,LINE,POINT");
        textRenderer.addStr2D(125,20,"Souradnice " +popisShaderu);
        textRenderer.addStr2D(width - 90, height - 3, " (c) PGRF UHK");

        textRenderer.draw();
        glEnable(GL_DEPTH_TEST);
    }

    @Override
    public GLFWKeyCallback getKeyCallback() {
        return keyCallback;
    }

    @Override
    public GLFWWindowSizeCallback getWsCallback() {
        return wsCallback;
    }

    @Override
    public GLFWMouseButtonCallback getMouseCallback() {
        return mbCallback;
    }

    @Override
    public GLFWCursorPosCallback getCursorCallback() {
        return cpCallbacknew;
    }

    @Override
    public GLFWScrollCallback getScrollCallback() {
        return scrollCallback;
    }

    @Override
    public void dispose() {
    }

    private GLFWWindowSizeCallback wsCallback = new GLFWWindowSizeCallback() {
        @Override
        public void invoke(long window, int w, int h) {
            if (w > 0 && h > 0) {
                width = w;
                height = h;
                double ratio = (double) height / width;
                projection = new Mat4PerspRH(Math.PI / 4, ratio, 1, 20);
                ortho = new Mat4OrthoRH(-ratio * 10, ratio * 20, 1, 10);
                if (textRenderer != null) {
                    textRenderer.resize(width, height);
                    textRenderer.draw();

                }
            }
        }
    };

    private GLFWMouseButtonCallback mbCallback = new GLFWMouseButtonCallback() {
        @Override
        public void invoke(long window, int button, int action, int mods) {
            isMousePressed = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS;
            isMousePressed2 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS;


            if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
                isMousePressed = true;
                DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                glfwGetCursorPos(window, xBuffer, yBuffer);
                mx = xBuffer.get(0);
                my = yBuffer.get(0);
            }

            if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE) {
                isMousePressed2 = false;
                DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                glfwGetCursorPos(window, xBuffer, yBuffer);
                double x = xBuffer.get(0);
                double y = yBuffer.get(0);
                camera = camera.addAzimuth(Math.PI * (mx - x) / width)
                        .addZenith(Math.PI * (my - y) / width);
                mx = x;
                my = y;
            }

            if (button == GLFW_MOUSE_BUTTON_2 && action == GLFW_PRESS) {
                isMousePressed2 = true;
                DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                glfwGetCursorPos(window, xBuffer, yBuffer);
                mx = xBuffer.get(0);
                my = yBuffer.get(0);
            }

            if (button == GLFW_MOUSE_BUTTON_2 && action == GLFW_RELEASE) {
                isMousePressed = false;
                DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                glfwGetCursorPos(window, xBuffer, yBuffer);
                double x = xBuffer.get(0);
                double y = yBuffer.get(0);
                cameraLight = cameraLight.addAzimuth(Math.PI * (mx - x) / width)
                        .addZenith(Math.PI * (my - y) / width);
                mx = x;
                my = y;
            }

        }
    };

    private GLFWCursorPosCallback cpCallbacknew = new GLFWCursorPosCallback() {
        @Override
        public void invoke(long window, double x, double y) {
            if (isMousePressed) {
                camera = camera.addAzimuth(Math.PI * (mx - x) / width)
                        .addZenith(Math.PI * (my - y) / width);

                mx = x;
                my = y;
            } else if (isMousePressed2) {
                cameraLight = cameraLight.addAzimuth(Math.PI * (mx - x) / width)
                        .addZenith(Math.PI * (my - y) / width);
                mx = x;
                my = y;

            }
        }
    };

    protected GLFWScrollCallback scrollCallback = new GLFWScrollCallback() {
        @Override
        public void invoke(long window, double dx, double dy) {
            mouseWheel = (int) (mouseWheel + dy);
            if (mouseWheel > 8) {
                mouseWheel = 8;
            } else if (mouseWheel < 0) {
                mouseWheel = 0;
            }
            switch (mouseWheel){
                case 0:
                    popisBarvy = "Normala";
                    break;
                case 1:
                    popisBarvy = "Souradnice.xyz";
                    break;
                case 2:
                    popisBarvy = "Barva";
                    break;
                case 3:
                    popisBarvy = "Textura";
                    break;
                case 4:
                    popisBarvy = "Hloubka";
                    break;
                case 5:
                    popisBarvy = "Textura.xy";
                    break;
                case 6:
                    popisBarvy = "PerVertex";
                    break;
                case 7:
                    popisBarvy = "PerPixel";
                    break;
                case 8:
                    popisBarvy = "Stiny + reflektor";
                    break;
            }
            System.out.println(mouseWheel);
        }
    };


    private GLFWKeyCallback keyCallback = new GLFWKeyCallback() {
        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
                // We will detect this in our rendering loop
                glfwSetWindowShouldClose(window, true);
            if (action == GLFW_RELEASE) {
            }
            if (action == GLFW_PRESS || action == GLFW_REPEAT) {
                switch (key) {
                    case GLFW_KEY_W:
                        camera = camera.forward(0.1);
                        glUniform3f(locLightFromCamera, (float) camera.getPosition().getX(), (float) camera.getPosition().getY(), (float) camera.getPosition().getZ());
                        System.out.println(cameraLight.toString());
                        break;
                    case GLFW_KEY_A:
                        camera = camera.left(0.1);
                        glUniform3f(locLightFromCamera, (float) camera.getPosition().getX(), (float) camera.getPosition().getY(), (float) camera.getPosition().getZ());
                        break;
                    case GLFW_KEY_D:
                        camera = camera.right(0.1);
                        glUniform3f(locLightFromCamera, (float) camera.getPosition().getX(), (float) camera.getPosition().getY(), (float) camera.getPosition().getZ());
                        break;
                    case GLFW_KEY_S:
                        camera = camera.backward(0.1);
                        glUniform3f(locLightFromCamera, (float) camera.getPosition().getX(), (float) camera.getPosition().getY(), (float) camera.getPosition().getZ());
                        break;
                    case GLFW_KEY_E:
                        if (changeOfShape == 0) changeOfShape = 1;
                        else changeOfShape = 0;
                        break;
                    case GLFW_KEY_SPACE:
                        camera = camera.up(0.1);
                        glUniform3f(locLightFromCamera, (float) camera.getPosition().getX(), (float) camera.getPosition().getY(), (float) camera.getPosition().getZ());
                        break;
                    case GLFW_KEY_LEFT_SHIFT:
                        camera = camera.down(0.1);
                        glUniform3f(locLightFromCamera, (float) camera.getPosition().getX(), (float) camera.getPosition().getY(), (float) camera.getPosition().getZ());
                        break;
                    case GLFW_KEY_O:
                        OrthoOrProjection = true;
                        ortho = new Mat4OrthoRH(10, 20, 1, 10);
                        hybridMatForView = ortho;
                        break;
                    case GLFW_KEY_P:
                        OrthoOrProjection = false;
                        projection = new Mat4PerspRH(Math.PI / 3,
                                Window.HEIGHT / (float) Window.WIDTH, 1, 20);
                        hybridMatForView = projection;
                        break;
                    case GLFW_KEY_F:
                       polygonMode = 0;
                        break;
                    case GLFW_KEY_G:
                        polygonMode = 1;
                        break;
                    case GLFW_KEY_H:
                        polygonMode = 2;
                        break;
                    case GLFW_KEY_C:
                        animation = !animation;
                        break;
                    case GLFW_KEY_F1:
                        shaderProgramViewer = ShaderUtils.loadProgram("/kartez/start");
                        shaderProgramLight = ShaderUtils.loadProgram("/kartez/light");
                        popisShaderu = "kartezske";
                        calculateShaderLocs(shaderProgramViewer, shaderProgramLight);
                        mouseWheel = 8;
                        break;
                    case GLFW_KEY_F2:
                        shaderProgramViewer = ShaderUtils.loadProgram("/sferika/sferika");
                        shaderProgramLight = ShaderUtils.loadProgram("/sferika/lightSferika");
                        popisShaderu = "sfericke";
                        calculateShaderLocs(shaderProgramViewer, shaderProgramLight);
                        mouseWheel = 8;
                        break;
                    case GLFW_KEY_F3:
                        shaderProgramViewer = ShaderUtils.loadProgram("/cylindrika/cylindrika");
                        shaderProgramLight = ShaderUtils.loadProgram("/cylindrika/lightCylindrika");
                        popisShaderu = "cylindricke";
                        calculateShaderLocs(shaderProgramViewer, shaderProgramLight);
                        mouseWheel = 8;
                        break;
                    case GLFW_KEY_F4:
                        shaderProgramViewer = ShaderUtils.loadProgram("/perPixelVsPerVertex");
                        calculateShaderLocs(shaderProgramViewer, shaderProgramLight);
                        break;

                }
            }
        }
    };

    /**
     * Vypocita jednotlive promene pro shadery
     *
     * @param shaderProgramViewer shader porgram pro pozorovatele
     * @param shaderProgramLight  shader program pro svetlo
     */
    private void calculateShaderLocs(int shaderProgramViewer, int shaderProgramLight) {

        locView = glGetUniformLocation(shaderProgramViewer, "view");
        locProjection = glGetUniformLocation(shaderProgramViewer, "projection");
        locTime = glGetUniformLocation(shaderProgramViewer, "time");
        locType = glGetUniformLocation(shaderProgramViewer, "type");
        locLightVP = glGetUniformLocation(shaderProgramViewer, "lightViewProjection");


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        locLightFromCameraLight = glGetUniformLocation(shaderProgramViewer, "lightFromCameraLight");
        locChangeOfShape = glGetUniformLocation(shaderProgramViewer, "changeOfShape");
        locmouseWheel = glGetUniformLocation(shaderProgramViewer, "mouseWheel");
        locReflektor = glGetUniformLocation(shaderProgramViewer, "reflektor");

        //--------------------------------------------------------------------------------------------------------------

        locChangeOfShapeLight = glGetUniformLocation(shaderProgramLight, "changeOfShapeLight");


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        locViewLight = glGetUniformLocation(shaderProgramLight, "view");
        locProjectionLight = glGetUniformLocation(shaderProgramLight, "projection");
        locTimeLight = glGetUniformLocation(shaderProgramLight, "time");
        locTypeLight = glGetUniformLocation(shaderProgramLight, "type");

    }

}