package main;

import lwjglutils.OGLBuffers;

/**
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2019-09-02
 */
public class GridFactory {

    public static OGLBuffers generateGrid(int m, int n) {

        float[] vb = new float[m * n * 2];
        int[] ib = new int[(m - 1) * (n - 1) * 3 * 2];

        int index = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {

                vb[index++] = (float) j / (m - 1);

                vb[index++] = (float) i / (n - 1);


            }

        }

        int index2 = 0;
        for (int j = 0; j < n - 1; j++) {
            for (int i = 0; i < m - 1; i++) {
                ib[index2++] = (j * m) + i;
                ib[index2++] = (j * m) + i + 1;
                ib[index2++] = (j * m) + i + m;


                ib[index2++] = (j * m) + i + m;
                ib[index2++] = (j * m) + i + 1;
                ib[index2++] = (j * m) + i + m + 1;
            }
        }

        OGLBuffers.Attrib[] attribs = new OGLBuffers.Attrib[]{
                new OGLBuffers.Attrib("inPosition", 2)

        };
        return new OGLBuffers(vb, attribs, ib);
    }


    public static OGLBuffers generateGridStrip(int m, int n) {

        float[] vb = new float[m * n * 2];
        int[] ib = new int[2 * m * (n)-2];

        int index = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {

                vb[index++] = (float) j / (m - 1);

                vb[index++] = (float) i / (n - 1);


            }

        }

        int index2 = 0;
        for (int j = 0; j < n - 1; j++) {
            for (int i = 0; i < m; i++) {
                if (j % 2 == 0) {
                    if (i == m - 1) {
                        ib[index2++] = (j*m) + i ;
                        ib[index2++] = (j*m) + n + i ;
                        ib[index2++] = (j*m) + n + i ;
                        ib[index2++] = (j*m) + n + i ;

                    } else {
                        ib[index2++] = (j * m) + i;
                        ib[index2++] = (j * m) + i + n;
                    }

                } else {
                    if (i == m - 1) {
                        ib[index2++] = (j*m) + n;
                        ib[index2++] = (j*m);
                        ib[index2++] = (j*m);
                        ib[index2++] = (j*m);
                    } else {
                        ib[index2++] = (j * m) + 2 * m - 1 - i;
                        ib[index2++] = (j * m) + m - 1 - i;
                    }
                }
            }
        }


        OGLBuffers.Attrib[] attribs = new OGLBuffers.Attrib[]{
                new OGLBuffers.Attrib("inPosition", 2)

        };
        return new OGLBuffers(vb, attribs, ib);
    }



}
